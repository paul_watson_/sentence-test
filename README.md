Small project that checks to see if it a valid sentence and meets the 
following criteria

String starts with a capital letter
String has an even number of quotation marks
String ends with a period character “."
String has no period characters other than the last character
Numbers below 13 are spelled out (”one”, “two”, "three”, etc…)
