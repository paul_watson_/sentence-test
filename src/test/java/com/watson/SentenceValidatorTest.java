package com.watson;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Test class for SentenceValidator
 */
public class SentenceValidatorTest {


    @Test
    public void isValidSentenceNullParamter() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse("Sentence is null", actual);
    }

    @Test
    public void isValidSentenceEmptyString() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse("Sentence is empty", actual);
    }

    @Test
    public void isValidSentenceWhitespace() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "   ";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceMultipleFullStops() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said \"hello Mr. lazy dog\".";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceNoCapitalLetter() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "the quick brown fox said \"hello Mr lazy dog\".";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceInvalidQuotes() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said \"hello Mr lazy dog.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceInvalidMultipleQuotes() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said \"hello Mr lazy dog\" the dog said woof\" .";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceInvalidMultipleNumber() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "This is a number of numbers 14,13,12  .";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void invalidSentenceNumberLessThan13() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "One lazy dog is too few, 12 is too many.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertFalse(sentence, actual);
    }

    @Test
    public void validSentenceMultipleQuote() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said \"hello Mr lazy dog\" the dog said  \"woof\" .";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }


    @Test
    public void validSentence1() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said \"hello Mr lazy dog\".";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }

    @Test
    public void validSentence2() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "The quick brown fox said hello Mr lazy dog.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }

    @Test
    public void validSentence3() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "One lazy dog is too few, 13 is too many.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }

    @Test
    public void validSentence4() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "One lazy dog is too few, thirteen is too many.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }

    @Test
    public void validSentenceMultipleNumber() {

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "Here are my numbers, 13,14,15.";
        boolean actual = sentenceValidator.isValidSentence(sentence);

        assertTrue(sentence, actual);
    }

}