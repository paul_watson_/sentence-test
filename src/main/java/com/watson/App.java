package com.watson;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args) {
        // write your code here

        SentenceValidator sentenceValidator = new SentenceValidator();
        String sentence = "M.";

        boolean isSentence = sentenceValidator.isValidSentence(sentence);

        if (isSentence){
            System.out.println(sentence + " - is a valid sentence");
        }



    }
}
