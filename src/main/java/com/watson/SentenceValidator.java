package com.watson;

import org.apache.log4j.Logger;

/**
 * Sentence validator class
 *
 */
public class SentenceValidator {

    private static final Logger logger = Logger.getLogger(SentenceValidator.class);

    private static final String PERIOD = ".";
    private static final String DOUBLE_QUOTE = "\"";
    private static final String SEPARATOR = " ";
    private static final int NUMBER_THRESHOLD = 13;

    /**
     * Checks to see if it a valid sentence and meets the following c
     * 
     * String starts with a capital letter
     * String has an even number of quotation marks
     * String ends with a period character “."
     * String has no period characters other than the last character
     * Numbers below 13 are spelled out (”one”, “two”, "three”, etc…)
     *
     * @param pSentenceToTest the sentence to test
     * @return true - it is a valid sentence
     *  false - the sentence has failed of one of the conditions
     */
    public boolean isValidSentence( String pSentenceToTest){

        // the objective is to exit quickly if one of the conditions are not met

        // check to see if the string is null or blank
        if ((pSentenceToTest == null) || (pSentenceToTest.isEmpty())) {

            if (logger.isDebugEnabled()){
                logger.debug( " The sentence is null or empty" );
            }
            return false;
        }

        boolean startsWithACapital = Character.isUpperCase(pSentenceToTest.charAt(0));
        boolean endsWithAPeriod = pSentenceToTest.charAt(pSentenceToTest.length()-1) == PERIOD.charAt(0);
        // if it doesn't start with a capital or end with a period then return false
        if (! ( startsWithACapital && endsWithAPeriod)) {
            if (logger.isDebugEnabled()){
                logger.debug( pSentenceToTest + " : doesn't start with a capital or end with a ful stop" );
            }
            return false;
        }

        if (getNumberOfOccurrences(PERIOD, pSentenceToTest) > 1){

            if (logger.isDebugEnabled()){
                logger.debug( pSentenceToTest + " : has more than one full stop" );

            }
            return false;
        }

        // check the double quote count
        if ( (getNumberOfOccurrences(DOUBLE_QUOTE, pSentenceToTest) % 2) != 0){

            if (logger.isDebugEnabled()){
                logger.debug( pSentenceToTest + " : has an invalid number of double quotes" );

            }
            return false;
        }

        // use a regular expression to replace everything that
        // isn't a number with a space.
        String numbersInSentence = pSentenceToTest.replaceAll("[^-?0-9]+", SEPARATOR).trim();

        if (logger.isDebugEnabled()){
            logger.debug("The numbers in the sentence are : {" + numbersInSentence + "}" );
        }


        String[] words = numbersInSentence.split(SEPARATOR);
        for(String currentWord : words){

            if( isANumberLessThan(NUMBER_THRESHOLD, currentWord)){
                return false;
            }
        }

        return true;

    }

    /**
     * get the Number Of Occurrences of a string in a sentence
     *
     * @param pStringToCount - the string to find
     * @param pSentenceToTest the sentence to test
     * @return the number of occurances
     */
    private int getNumberOfOccurrences(String pStringToCount, String pSentenceToTest){

        return (pSentenceToTest.length() - pSentenceToTest.replace(pStringToCount, "").length()) / pStringToCount.length();
    }

    /**
     * check to see if the word passed in
     * is a number less then number to check
     *
     * @param pNumberToCheck -the number to check
     * @param pWord - the number as a string.
     * @return true it is a number less than number to check;
     *          false it is not a number or a number less than 12
     */
    private boolean isANumberLessThan(int pNumberToCheck, String pWord){

        try {

            // converting to int since number with decimal points
            // would invalidate the sentence.
            int value = Integer.parseInt(pWord);
            if (value < pNumberToCheck){
                return true;
            }

        } catch (NumberFormatException e){
            if (logger.isDebugEnabled()){
                logger.debug("The number is not an integer  : {" + pNumberToCheck + "}" );
            }
            return false;
        }

        return false;

    }
}
